#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import json
import requests
from bottle import route, run, static_file, view


#### 定数
# プロジェクトルートのディレクトリの絶対パスを取得
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
# publickディレクトリの絶対パスを取得
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))
# APIのURL
# raw文字列として文字列定義したものを定数の格納
NICO_API_URL = r"http://api.search.nicovideo.jp/api/"

@route('/public/<filepath:path>')
def static_root(filepath):
    """
    公開ディレクトリのルーティング設定
    ファイルパスを返す
    """
    return static_file(filepath, root=STATIC_ROOT)

@route('/')
@route('/<name>')
@route('/<name>/')
@route('/<name>/<lines:int>')
@route('/<name>/<lines:int>/')
@view('index')
def index(name="ニコニコ動画", lines=10):
    """
    * indexコントローラー
    ルーティング（URLの設定）と
    レンダリングするテンプレートの定義を
    デコレータで行う
    """
    # デバッカー
    #import pdb; pdb.set_trace()
    # タイトルを変数に格納
    title = "「{0}」のニコニコ動画を検索".format(name)
    # エラー時の返却データ
    error_data = {"error": "動画が見つかりません"}
    # 結果データ
    result_data = None

    # リクエストデータを生成
    qy = {
      "query": name,
      "service": [
        "video"
      ],
      "search": [
        "title"
      ],
      "join": [
        "cmsid",
        "title",
        "view_counter"
      ],
      "filters": [
        {
          "type":"equal",
          "field":"music_download",
          "value": True
        }
      ],
      "from": 0,
      "size": lines,
      "sort_by": "view_counter",
      "issuer": "apiguide",
      "reason": "ma10"
    }
    # debug
    #print(qy)

    # APIにjsonデータを送りレスポンスをもらう
    res = requests.post(NICO_API_URL, json.dumps(qy).encode())

    if res.status_code != requests.codes.ok:
        result_data = error_data
    else:
        js_data = res.text.replace('\n', ',').rstrip(',')

        # jsonデータを配列に変換
        data = json.loads("[{0}]".format(js_data))

        # データの正常性調査
        for d in data:
            vals = d.get('values')
            print(d.get('type'))
            if d.get('type') == "hits":
                result_data = vals
                break
            else:
                result_data = error_data

        print(result_data)

    return {
        # titleをviewに送る
        'title': title,
        # データだけの配列をviewに送る
        'data': result_data,
        'name': name,
        'lines': lines
    }


# このif文はスクリプトファイルとして呼ばれた場合のみif内を実行する
# 決まり文句です。
if __name__ == '__main__':
    from bottle import TEMPLATES
    TEMPLATES.clear()
    # bottleのwebサーバーを起動
    run(host='localhost', port=8080, debug=True, reloader=True)


#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import json
from urllib import request
from bottle import route, run, static_file, view


#### 定数
# プロジェクトルートのディレクトリの絶対パスを取得
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
# publickディレクトリの絶対パスを取得
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))
# APIのURL
# raw文字列として文字列定義したものを定数の格納
NICO_API_URL = r"http://api.search.nicovideo.jp/api/tag/"

@route('/public/<filepath:path>')
def static_root(filepath):
    """
    公開ディレクトリのルーティング設定
    ファイルパスを返す
    """
    return static_file(filepath, root=STATIC_ROOT)

@route('/')
@route('/<tag>')
@route('/<tag>/')
@route('/<tag>/<lines:int>')
@route('/<tag>/<lines:int>/')
@view('index')
def index(tag="ニコニコ動画", lines=10):
    """
    * indexコントローラー
    ルーティング（URLの設定）と
    レンダリングするテンプレートの定義を
    デコレータで行う
    """
    # デバッカー
    # import pdb; pdb.set_trace()
    # タイトルを変数に格納
    title = "「{0}」のニコニコタグを検索".format(tag)
    # エラー時の返却データ
    error_data = ["タグが見つかりません"]

    # リクエストデータを生成
    qy = {
        "query": tag,
        "service": [
            "tag_video"
        ],
        "size": lines,
        "from": 0,
        "issuer": "apiguide",
        "reason": "ma10"
    }
    # debug
    #print(qy)

    # jsonデータを生成し、デフォルトのutf-8に変換
    js_data = json.dumps(qy).encode()

    # with文を使ってclose()しないでもメモリを開放させる
    # APIにjsonデータを送りレスポンスをもらう
    with request.urlopen(NICO_API_URL, js_data) as r:
        # 返ってきたjsonデータを全て読み込む
        content = r.read()

    # ユニコード文字列にjson_data文字列を変換
    _dec_data = content.decode()
    # 返ってきたjsonが不正な形式だったので、jsonデータだけを
    # 取り出す
    dec_data = _dec_data.split('\n')[0]
    # jsonデータを配列に変換
    data = json.loads(dec_data)

    # データの正常性調査
    values = data.get('values')
    result_data = []
    # データがない場合
    if values is None:
        result_data = error_data
    else:
        for val in values:
            # 連想配列にtagキーがない場合
            if val.get('tag') is None:
                result_data = error_data
            else:
                # result_dataの配列に追加
                result_data.append(val.get('tag'))

    return {
        # titleをviewに送る
        'title': title,
        # データだけの配列をviewに送る
        'data': result_data,
        'tag': tag,
        'lines': lines
    }


# このif文はスクリプトファイルとして呼ばれた場合のみif内を実行する
# 決まり文句です。
if __name__ == '__main__':
    # bottleのwebサーバーを起動
    run(host='localhost', port=8080, debug=True, reloader=True)


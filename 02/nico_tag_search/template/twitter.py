#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from bottle import route, run, static_file, view


# 定数
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))

# 公開ディレクトリの設定
@route('/public/<filepath:path>')
def static_root(filepath):
    return static_file(filepath, root=STATIC_ROOT)

# コントローラー
@route('/')
@route('/<name>')
@route('/<name>/')
@view('index')
def index(name=''):
    if not name:
        name = "World"

    title = "Hello {0}!".format(name)

    return { 'title': title }


if __name__ == '__main__':
    run(host='localhost', port=8080, debug=True, reloader=True)

